﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System;
using System.Text;

[System.Serializable]
public class DamageInfo
{
    public int playerHP;
    public int enemyHP;
    public int playerDmg;
    public int enemyDmg;
    public string pHP;
    public string eHP;
    public string pDmg;
    public string eDmg;
    }
[System.Serializable]
public class StatsInfo
{
    public int wins;
    public int loses;
}

public class UI_control : MonoBehaviour {
    public UI_control Instance;
    private void Awake()
    {
        Instance = this;
    }

    private ClientBehavior client;
    public string nickname;

    public bool canPunch = false;
    public UnityEvent OnCanPunch;
    public bool inBattle = false;
    public UnityEvent OnInBattle;
    public UnityEvent OnDamageInfo;

    public DamageInfo lastDamages;
    public StatsInfo stats;

    #region OnScreen log
    [SerializeField]
    public static Text _console;
    private static int logCount = 0;
    public static void AddLog(string _str)
    {
        _console.text += _str + '\n';
        logCount++;
        if (logCount > 15)
        {
            string s = _console.text;
            s = s.Remove(0, s.IndexOf('\n') + 1);
            _console.text = s;
            logCount--;
        }
    }
    public static void ClearLog()
    {
        if (_console)
            _console.text = "";
    }
    #endregion

    private void Start()
    {
        // получаем сокет
        client = ClientBehavior.Instance;
        // назначаем события сокета
        client.OnConnectEvent += ConnectEvent;
        client.OnDisconnectEvent += DisconnectEvent;
        client.OnMessageEvent += ProcessMessages;
        // находим и задаём Text для работы лога
        UI_control._console = GameObject.FindGameObjectWithTag("console").GetComponent<Text>();
    }

    public Button[] lobbyButtons;
    public Button[] battleButtons;
    public GameObject[] chatObjects;
    public Button[] connectButtons;

    public InputField fieldNickname;
    public InputField fieldChattext;

    // Срабатывает при коннекте
    public void ConnectEvent()
    {
        foreach (Button b in connectButtons)
            b.interactable = !b.interactable;
        foreach (Button b in lobbyButtons)
            b.interactable = !b.interactable;
        foreach (var g in chatObjects)
            g.SetActive(!g.active);
    }

    // Срабатывает при дисконнекте
    public void DisconnectEvent()
    {
        foreach (Button b in connectButtons)
            b.interactable = !b.interactable;
        foreach (Button b in lobbyButtons)
            b.interactable = !b.interactable;
        foreach (var g in chatObjects)
            g.SetActive(!g.active);
    }
    
    // Кнопка Connect
    public void ButtonConnect()
    {
        client.Connect();
        UI_control.ClearLog();
    }
    
    // Кнопка Disconnect
    public void ButtonDisconnect()
    {
        client.Disconnect();
    }

    // Обработка входящих сообщений
    // Разбиение их по JSON пакетам
    public void ProcessMessages(string value)
    {
        string[] result = value.Split(']');
        foreach (string s in result)
        {
            if (s.Length > 4)
            {
//                print(s + ']');
                OnMessage(s + ']');
            }
        }
    }

    // Обработка одного JSON пакета
    public void OnMessage(string value)
    {
        JSONObject j = new JSONObject(value);
        if (j[0].str == "SYSTEM")
        {
            if (j[1].str == "ID")
            {
                client.clientID = Convert.ToInt32(j[2].str);
                nickname = fieldNickname.text;
                j.Clear();
                j.Add("SYSTEM");
                j.Add("NICKNAME");
                j.Add(client.clientID.ToString());
                j.Add(nickname);
                client.Send(j.ToString());
            }
            if (j[1].str == "USERS")
                if (Convert.ToInt32(j[2].str) == client.clientID)
                {
                    UI_control.AddLog(j[3].str);
                }
        }
        if (j[0].str == "MESSAGE")
        {
            if (j[1].str != "SERVER")
            {
                string s = j[1].str + " : " + j[3].str;
                UI_control.AddLog(s);
            }
            else
            {
                string s = "Server message - "+j[2].str;
                UI_control.AddLog(s);
            }
            j.Clear();
            return;
        }
        if (j[0].str == "BATTLE")
        {
            if (j[1].str == "TRUE")
            {
                inBattle = true;
                OnInBattle.Invoke();
            }
            if (j[1].str == "FALSE")
            {
                inBattle = false;
                OnInBattle.Invoke();
            }

            if (j[1].str == "CAN_PUNCH")
                if (j[2].str == "1")
                {
                    canPunch = true;
                    OnCanPunch.Invoke();
                }
                else
                {
                    canPunch = false;
                    OnCanPunch.Invoke();
                }
            if (j[1].str == "DAMAGE")
            {
                lastDamages.playerHP = Convert.ToInt32(j[2].str);
                lastDamages.enemyHP = Convert.ToInt32(j[3].str);
                lastDamages.playerDmg = Convert.ToInt32(j[4].str);
                lastDamages.enemyDmg = Convert.ToInt32(j[5].str);
                lastDamages.pHP = j[2].str;
                lastDamages.eHP = j[3].str;
                lastDamages.pDmg = j[4].str;
                lastDamages.eDmg = j[5].str;
                OnDamageInfo.Invoke();
            }
            if (j[1].str == "PLAYER_PUNCH") {
                UI_control.AddLog("Player PUNCH enemy");
            }
            if (j[1].str == "ENEMY_PUNCH") {
                UI_control.AddLog("Enemy PUNCH player");
            }

        }
        if (j[0].str == "STATUS")
        {
            if (j[1].str == "VICTORY") { }
            if (j[1].str == "DEFEAT") { }
            if (j[1].str == "STATS") {
                int.TryParse(j[2].str, out stats.loses);
                int.TryParse(j[3].str, out stats.wins);
                UI_control.AddLog("Stats :: wins " + j[3].str + "  loses " + j[2].str);
            }
        }
    }

    // Кнопка списка игроков на сервере
    public void ButtonList()
    {
        JSONObject j = new JSONObject();
        j.Add("SYSTEM");
        j.Add("LIST");
        j.Add(client.clientID.ToString());
        client.Send(j.ToString());
    }

    // Отправка сообщения в чат
    public void TextSend()
    {
        string s = fieldChattext.text;
        if (s.Length < 1 || client.clientID < 0) // ignore empty strings
            return;

        JSONObject j = JSONObject.obj;
        j.Add("MESSAGE");
        j.Add(nickname);
        j.Add(client.clientID.ToString());
        j.Add(s);
        string ss = j.ToString();
        if (client.Send(j.ToString()))
        { }
            //inField.text = "";


    }

    // Кнопка Battle - начать бой на сервере
    public void ButtonBattle()
    {
        StartBattle();
        JSONObject j = JSONObject.obj;
        j.Clear();
        j.Add("BATTLE");
        j.Add("START_BATTLE");
        j.Add(client.clientID.ToString());
        client.Send(j.ToString());
    }

    // Метод вкл/выкл кнопок на UI при бое
    public void StartBattle()
    {
        if (inBattle)
        {
            foreach (Button b in battleButtons)
                b.interactable = true;
            foreach (Button b in lobbyButtons)
                b.interactable = false;
        }
        else
        {
            foreach (Button b in battleButtons)
                b.interactable = false;
            foreach (Button b in lobbyButtons)
                b.interactable = true;
        }
    }

    // Метод проверки возможности игроку ударить
    // НЕ ИСПОЛЬЗУЕТСЯ, СЕРВЕР САМ ШЛЕТ КОГДА МОЖНО БИТЬ
    public void CheckPunchStatus()
    {
        JSONObject j = JSONObject.obj;
        j.Add("BATTLE");
        j.Add("CAN_PUNCH");
        j.Add(client.clientID.ToString());
        client.Send(j.ToString());
    }

    // Запрос на удар на сервер
    public void ButtonPunch()
    {
        JSONObject j = JSONObject.obj;
        j.Clear();
        j.Add("BATTLE");
        j.Add("PUNCH");
        j.Add(client.clientID.ToString());
        client.Send(j.ToString());
        canPunch = false;
        CanPunch();
    }

    // Метод вкл/выкл кнопки удара на UI
    public void CanPunch()
    {
        foreach (Button b in battleButtons)
            b.interactable = canPunch;
    }

    // Метод вывода здоровья на UI
    public void ShowDamage()
    {
        UI_control.AddLog("Player: " + lastDamages.pHP + " (-" + lastDamages.eDmg + ")");
        UI_control.AddLog("Enemy: " + lastDamages.eHP + " (-" + lastDamages.pDmg + ")");
    }
}
