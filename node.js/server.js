var net = require('net');
const { StringDecoder } = require('string_decoder');
const decoder = new StringDecoder('utf8');

var _port = 8000;
var IDcounter = -1;
var battleCounter = -1;
var DeltaTime = 100;

function RandomInt(min, max){
	return Math.floor(Math.random() * (max - min + 1)) + min;
}
function RandomFloat(min, max){
	return (Math.random() * (max - min + 1)) + min;
}

// Add remove function for arrays
Array.prototype.remove = function(e) {
	for (var i = 0; i < this.length; i++) {
	  if (e == this[i]) { return this.splice(i, 1); }
	}
  };

var clients = [];

function Client(_socket, _id){
	this.nickname = '<noname>';
	this.socket = _socket;
	this.ID = _id;
	this.wins = 0;
	this.loses = 0;
	this.battle = null;
	this.inBattle = false;
}

var battles = [];
function Battle(_client){
	battleCounter++;
	this.ID = battleCounter;
	this.player = _client;
	this.player.battle = this;
	this.player.inBattle = true;
	this.playerHP = 100;
	this.enemyHP = 100;
	this.minDamage = 1;
	this.maxDamage = 8;
	this.punchCooldown = 1500;
	this.punchTimerPlayer = 0.0;
	this.punchTimerEnemy = 0.0;
	this.lastDamagePlayer = 0;
	this.lastDamageEnemy = 0;
	this.Process = function(_deltaTime)	{
		if (this.punchTimerPlayer > 0.0)
		{
			this.punchTimerPlayer -= _deltaTime;
			if (this.punchTimerPlayer <= 0.0 ) 
			{
				this.punchTimerPlayer = 0.0;
				this.player.socket.write('["BATTLE","CAN_PUNCH","1"]');
				console.log('Player '+this.player.nickname+' can punch');

			}
		}
		if (this.punchTimerEnemy > 0.0)
		{
			this.punchTimerEnemy -= _deltaTime;
			if (this.punchTimerEnemy <= 0.0)
				this.punchTimerEnemy = 0.0;
		}
		if (this.punchTimerEnemy === 0.0){
			this.EnemyPunch();
		}
	}
	this.EnemyPunch = function() {
		var dmg = RandomInt(this.minDamage, this.maxDamage);
		this.playerHP -= dmg;
		this.lastDamageEnemy = dmg;
		this.punchTimerEnemy = this.punchCooldown;
		this.player.socket.write('["BATTLE","ENEMY_PUNCH"]');
		if (this.playerHP <= 0.0)
		{
			this.SendDefeat();
			battles.remove(this);
		}
		this.player.socket.write('["BATTLE","DAMAGE","'+
		this.playerHP+'","'+
		this.enemyHP+'","'+
		this.lastDamagePlayer+'","'+
		this.lastDamageEnemy+'"]');
	}
	this.PlayerPunch = function()	{
		if (this.punchTimerPlayer == 0.0) {
		 this.punchTimerPlayer = this.punchCooldown;
		 var dmg = RandomInt(this.minDamage, this.maxDamage);
		 this.enemyHP -= dmg;
		 this.lastDamagePlayer = dmg;
		 this.punchTimerPlayer = this.punchCooldown;
		if (this.enemyHP <= 0){
			this.SendVictory();
			battles.remove(this);
		}
		this.player.socket.write('["BATTLE","DAMAGE","'+
		this.playerHP+'","'+
		this.enemyHP+'","'+
		this.lastDamagePlayer+'","'+
		this.lastDamageEnemy+'"]');
		this.player.socket.write('["BATTLE","PLAYER_PUNCH"]');
		}
	}
	this.SendVictory = function ()	{
		this.player.socket.write('["STATUS","VICTORY"]');
		this.player.win++;
		this.player.inBattle = false;
		console.log('Player '+this.player.nickname+'['+this.player.ID+'] victory ('+this.player.wins+'/'+this.player.loses+')');
		battles.remove(this);
	}
	this.SendDefeat = function ()	{
		this.player.socket.write('["STATUS","DEFEAT"]');
		this.player.lose++;
		this.player.inBattle = false;
		console.log('Player '+this.player.nickname+'['+this.player.ID+'] lose ('+this.player.wins+'/'+this.player.loses+')');
		this.player.socket.write('["STATUS","STATS","'+this.player.loses+'","'+this.player.wins+'"]');
		battles.remove(this);
	}
}

setInterval(function(){
	console.log('Battles process:');
	battles.forEach(function(r)
	{
		console.log('  #'+r.ID+' tick');
		r.Process(DeltaTime);
	});
}, DeltaTime);

function ServerBroadcast(_text1, _text2){
	clients.forEach( function(c){
		c.socket.write(_text1);
	})
	//console.log(_text2);
	console.log('SEND: '+_text1);
}

var s = net.Server(function (socket) {
	console.log("Client connected");
	IDcounter++;
	_client = new Client(socket, ""+IDcounter);
	_client.socket.write('["SYSTEM","ID","'+ IDcounter +'"]');
	clients.push(_client);

	socket.on('data', function (msg) {
		var msg2 = ''+msg;
		msg2 = decoder.write(msg); // декодируем из UTF8
		console.log("Input: " + msg2); 
		data = "" + msg2;
		var j = JSON.parse(data);

		if (j[0] === 'MESSAGE'){
			ServerBroadcast(msg, msg2);
		}

		if (j[0] === 'SYSTEM'){
			if (j[1] === 'NICKNAME'){
				clients.forEach( function(c){
					if (c.ID === j[2]){
						c.nickname = j[3];
						console.log('ID #' + c.ID + ' set nick ' + c.nickname);
						ServerBroadcast('["MESSAGE","SERVER","User '+c.nickname+' is online"]', 'Message all: "User '+c.nickname+' is online"');
						/*
						clients.forEach( function(cc){
							cc.socket.write('["MESSAGE","SERVER","User '+c.nickname+' is online"]');
							console.log('Message all: "User '+c.nickname+' is online"' );
						}); */
					
					}
				})
			}
			if (j[1] === 'LIST'){
				var s = '["SYSTEM","USERS","'+j[2]+'","';
				clients.forEach( function(c){
					s += c.nickname+'\n';
				});
				s += '"]';
				console.log('== '+s);
				clients.forEach( function(c){
					if (c.ID === j[2]) {
						c.socket.write(s);
						console.log('Message to: ' + j[2] + ' (' + c.ID + ') ' + s);
					}
				});
	
				}
		}

		if (j[0] === 'BATTLE'){
			if (j[1] === 'CAN_PUNCH'){
				clients.forEach( function(c){
					if (c.ID === j[2]){
						if (c.battle.punchTimerPlayer === 0.0){
							c.socket.write('["BATTLE","CAN_PUNCH","1"]');
							console.log('Player '+cl.nickname+' can punch');
						}else{
							c.socket.write('["BATTLE","CAN_PUNCH","0"]');
							console.log('Player '+cl.nickname+'['+cl.ID+'] can\'t punch');
						}
					}
				})
			}
			if (j[1] === 'PUNCH'){
				battles.forEach(function(b){
					if (b.player.ID === j[2]){
						b.PlayerPunch();
						console.log('Battle #'+b.ID+' player "'+b.player.nickname+'" punch');
					}
				})
			}
			if (j[1] === 'START_BATTLE'){
				var cl;
				clients.forEach( function(c){
					if (c.ID === j[2]){
						cl = c;
					}
				})
				b = new Battle(cl);
				battles.push(b);
				cl.socket.write('["BATTLE","TRUE"]');
				ServerBroadcast('["MESSAGE","SERVER","Player '+b.player.nickname+' starts battle"]', 'Player '+cl.nickname+' start battle');
				console.log('Battle start #'+b.ID+' by player "'+b.player.nickname+'"');
				//console.log('Player '+cl.nickname+'['+cl.ID+'] start battle');
			}
		}
	});

	socket.on('error', function() {});

	socket.on('end', function () {
		console.log("Client disconnected");
		var i = clients.indexOf(socket);
		for (var i in clients) 
			if (clients[i].socket === socket)
				clients.splice(i, 1);

		for (var i in clients) {
			try {
				clients[i].socket.write("user left");
			}
			catch (ex) {}
		}
	});
});

s.listen(_port);

//* Handle ctrl+c event
process.on('SIGINT', function () {
	for (var i in clients) {
		clients[i].write("Server shutdown");
		clients[i].destroy();
	}
	console.log('Exiting properly');
	process.exit(0);
});

console.log("Chat Server listening " + s.address().address + ":" + s.address().port);